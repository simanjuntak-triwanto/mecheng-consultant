---
title: About
---
We are a group of academics and engineers that offer high-quality training and technical consultancy --- experimental and computational --- in Fluid Mechanics.

Our team of experienced CFD engineers can help you optimize your designs, troubleshoot problems, and improve your products' performance. 

We use the latest CFD software (OPenFoam) and techniques to provide you with accurate and reliable results. Our team of experienced CFD engineers can help you optimize your designs, troubleshoot problems, and improve your products' performance. We use the latest CFD software and techniques to provide you with accurate and reliable results.

CFD simulations can give you insights into the behavior of fluids that you simply can't get from physical testing. This can help you make better design decisions, reduce your development costs, and bring your products to market faster.

We're not just CFD experts, we're also your partners in success. We'll work closely with you to understand your needs and develop a CFD solution that meets your specific requirements.
